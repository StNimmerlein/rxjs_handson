import {Observable} from 'rxjs'

// ============== DELAYS ===============
// Advanced TODO: Create a function that returns an observable which emits the values of a given array with a specified delay
// Input: createSlowObservable(1000, 'a', 'b', 'c')
// Output: Observable that emits: (wait 1000ms) 'a' (wait 1000ms) 'b' (wait 1000ms) 'c'
// Can be done in (at least) three different ways. Can you find them?

function createSlowObservable(delayInMs: number, ...values: any[]): Observable<any> {
    return undefined
}

const obs$ = createSlowObservable(1000, 'a', 'b', 'c')

obs$.subscribe(console.log)
