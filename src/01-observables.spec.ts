import {Observable} from 'rxjs'

describe('observables basics', () => {
    it('create an observable for a single value', (done) => {
        // TODO: Create an observable that emits the desired string
        let a$: Observable<string>
        const stringToEmit = "Hello Observable"

        // ⬇️ Your code here
        // ⬆️ Your code here

        a$.subscribe(value => {
            expect(value).toBe(stringToEmit)
            done()
        })
    })

    it('create an observable for a list of values', (done) => {
        // TODO: Create an observable that emits the given values one after another
        let a$: Observable<string>
        const valuesToEmit = ["Hello", "Obs", "ervable", "!"]

        // ⬇️ Your code here
        // ⬆️ Your code here

        let index = 0
        a$.subscribe(value => {
            expect(value).toBe(valuesToEmit[index++])
            if (index == valuesToEmit.length) {
                done()
            }
        })
    })

    it('create an observable with callbacks ', (done) => {
        // Advanced TODO: You have a function 'doSomethingComplex' which takes some time. When it is done or fails, it will call a corresponding callback.
        // In this test it is set up that the first invocation succeeds while the second fails.
        // Create an observable that, when subscribed to, invokes this function and emits when the successCallback is called and errors when the errorCallback is called
        // The test creates two instances of this Observable and expects one to emit and one to error.

        let successful = true
        const doSomethingComplex = (successCallback, failedCallback) => {
            setTimeout(successful ? successCallback : failedCallback, 2000)
            successful = !successful
        }

        const createObservable = (): Observable<undefined> => {
            let a$: Observable<undefined>

            // ⬇️ Your code here
            // ⬆️ Your code here

            return a$
        }

        let counter = 0
        createObservable().subscribe(() => {
            if (++counter === 2) {
                done()
            }
        })
        createObservable().subscribe({
            error: () => {
                if (++counter === 2) {
                    done()
                }
            }
        })
    })
})
