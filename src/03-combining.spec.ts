import {combineLatest, concat, Observable, Subject, zip} from 'rxjs'
import {validateOutput} from './helpers'

describe('combining observables', () => {
    let numbers$: Subject<number>
    let characters$: Subject<string>

    // These two observables will emit values in this order (- means no value, | means completion, all numbers and characters are single-digit, so 12 means two values emitted (first 1, then 2)):
    //    numbers$: 0-1---1--238-3-5-82-46|
    // characters$: -a-bcd-ef---g-h-i--|

    beforeEach(() => {
        numbers$ = new Subject<number>()
        characters$ = new Subject<string>()
    })

    describe('concat observables', () => {
        it('numbers first', (done) => {
            let combinedObservable: Observable<number | string>

            combinedObservable = concat(numbers$, characters$)

            // TODO: Explain why the output is not '0 1 1 2 3 8 3 5 8 2 4 6 a b c d e f g h i' even though we concatenate the observables
            validateOutput(combinedObservable, '0 1 1 2 3 8 3 5 8 2 4 6', done)
            emitValues()
        })

        it('charactersFirst', (done) => {
            let combinedObservable: Observable<number | string>
            let expectedOutput: string

            combinedObservable = concat(characters$, numbers$)

            // TODO: Determine the output of this combinedObservable.
            // If the test fails, it will not tell you in detail where you were wrong.
            // However, if you are stuck and have absolutely no idea, change the 'hidden' parameter of validateOutput to false to see the difference between expected and actual.
            // But think for yourself first ;-)

            // ⬇️ Your code here
            // ⬆️ Your code here

            validateOutput(combinedObservable, expectedOutput, done, true)
            emitValues()
        })
    })
    it('merge observables', (done) => {
        let combinedObservable: Observable<number | string>
        const expectedOutput = '0 a 1 b c d 1 e f 2 3 8 g 3 h 5 i 8 2 4 6'

        // TODO: Create a new observable (using the two existing ones) that emits the expected output
        // ⬇️ Your code here
        // ⬆️ Your code here

        validateOutput(combinedObservable, expectedOutput, done)
        emitValues()
    })

    it('zip observables', (done) => {
        let expectedOutput: string
        const combinedObservable: Observable<[number, string]> = zip(numbers$, characters$)

        // TODO: Guess what the output of this combined observable is
        // Hint: If the observable would emit the two values "[3, 'a']" and "[5, 'g']" (both are tuples), the expectedOutput would be formatted as '3,a 5,g'
        // ⬇️ Your code here
        // ⬆️ Your code here

        validateOutput(combinedObservable, expectedOutput, done, true)
        emitValues()
    })

    it('fork observables', (done) => {
        const expectedOutput = '6,i'
        let combinedObservable: Observable<[number, string]>

        // TODO: Create a new observable (using the two existing ones) that emits the last value of each once both complete
        // ⬇️ Your code here
        // ⬆️ Your code here

        validateOutput(combinedObservable, expectedOutput, done)
        emitValues()
    })

    it('combine latest', (done) => {
        let expectedOutput: string
        const combinedObservable: Observable<[number, string]> = combineLatest([numbers$, characters$])

        // TODO: Guess what the output of this combined observable is
        // Hint: If the observable would emit the two values "[3, 'a']" and "[5, 'g']" (both are tuples), the expectedOutput would be formatted as '3,a 5,g'
        // ⬇️ Your code here
        // ⬆️ Your code here

        validateOutput(combinedObservable, expectedOutput, done, true)
        emitValues()
    })


    function emitValues() {
        numbers$.next(0)
        characters$.next('a')
        numbers$.next(1)
        characters$.next('b')
        characters$.next('c')
        characters$.next('d')
        numbers$.next(1)
        characters$.next('e')
        characters$.next('f')
        numbers$.next(2)
        numbers$.next(3)
        numbers$.next(8)
        characters$.next('g')
        numbers$.next(3)
        characters$.next('h')
        numbers$.next(5)
        characters$.next('i')
        numbers$.next(8)
        numbers$.next(2)
        characters$.complete()
        numbers$.next(4)
        numbers$.next(6)
        numbers$.complete()
    }
})