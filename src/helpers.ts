import {Observable} from 'rxjs'
import {map, reduce} from 'rxjs/operators'

export const validateOutput = (observable: Observable<any>, expectedOutput: string, doneCallback, hidden?: boolean) => {
    observable.pipe(
        map(value => value.toString()),
        reduce((a, b) => `${a} ${b}`)
    ).subscribe(result => {
        if (hidden) {
            expect(result === expectedOutput).toBeTruthy()
        } else {
            expect(result).toBe(expectedOutput)
        }
        doneCallback()
    })
}