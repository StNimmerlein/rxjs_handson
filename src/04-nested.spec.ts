import {Observable, Subject} from 'rxjs'
import {validateOutput} from './helpers'
import {concatMap, exhaustMap, mergeMap, switchMap} from 'rxjs/operators'

describe('nested observables', () => {
    let songs: {
        a$: Subject<string>,
        b$: Subject<string>,
        c$: Subject<string>,
        d$: Subject<string>,
        e$: Subject<string>
    }
    let keys$: Subject<keyof typeof songs>

    beforeEach(() => {
        // Hint: For the order of emitted values, see the method emitValues() below
        // keys$ emits 'a$', 'c$', 'b$', 'b$', 'a$', 'd$', 'e$
        keys$ = new Subject<keyof typeof songs>()
        songs = {
            // songs.a$ emits "Ich", "bin", "ein", "kleiner", "Teetopf"
            a$: new Subject<string>(),
            // songs.b$ emits "Freude", "schöner", "Götterfunken"
            b$: new Subject<string>(),
            // songs.c$ emits "Da", "simma", "dabei", "das", "is", "pri-hi-ma"
            c$: new Subject<string>(),
            // songs.d$ emits "Wind", "nordost", "Startbahn", "03"
            d$: new Subject<string>(),
            // songs.e$ emits "The", "End"
            e$: new Subject<string>()
        }
    })

    // TODO: For each test, guess the output of the observable obs$.
    // If the output was "Ich" -> "bin" -> "ein", then the expected output would be "Ich bin ein"
    // As in the previous tasks, when you are stuck, change the hidden flag of validateOutput to false to see the direct comparison between expected and actual

    describe('switchMap', () => {
        it('switches context', (done) => {
            let obs$: Observable<string>
            let expectedOutput: string

            obs$ = keys$.pipe(switchMap(key => songs[key]))

            // ⬇ Your code here
            // ⬆️ Your code here

            validateOutput(obs$, expectedOutput, done, true)
            emitValues()
        })
    })

    describe('concatMap', () => {
        it('concatenates outputs', (done) => {
            let obs$: Observable<string>
            let expectedOutput: string

            obs$ = keys$.pipe(concatMap(key => songs[key]))

            // ⬇ Your code here
            // ⬆️ Your code here

            validateOutput(obs$, expectedOutput, done, true)
            emitValues()
        })
    })

    describe('mergeMap', () => {
        it('merges outputs', (done) => {
            let obs$: Observable<string>
            let expectedOutput: string

            obs$ = keys$.pipe(mergeMap(key => songs[key]))

            // ⬇ Your code here
            // ⬆️ Your code here

            validateOutput(obs$, expectedOutput, done, true)
            emitValues()
        })
    })

    describe('exhaustMap', () => {
        it('merges outputs', (done) => {
            let obs$: Observable<string>
            let expectedOutput: string

            obs$ = keys$.pipe(exhaustMap(key => songs[key]))

            // ⬇ Your code here
            // ⬆️ Your code here

            validateOutput(obs$, expectedOutput, done, true)
            emitValues()
        })
    })

    function emitValues() {
        songs.a$.next("Ich")
        songs.a$.next("bin")
        keys$.next('a$')
        songs.b$.next("Freude")
        songs.c$.next("Da")
        songs.d$.next("Wind")
        songs.c$.next("simma")
        keys$.next('c$')
        songs.b$.next("schöner")
        songs.a$.next("ein")
        songs.c$.next("dabei")
        keys$.next('b$')
        songs.d$.next("nordost")
        songs.b$.next("Götterfunken")
        keys$.next('b$')
        songs.c$.next("das")
        keys$.next('a$')
        songs.b$.complete()
        keys$.next('d$')
        songs.a$.next("kleiner")
        songs.d$.next("Startbahn")
        songs.c$.next("is")
        songs.a$.next("Teetopf")
        songs.a$.complete()
        songs.d$.next("03")
        keys$.next('e$')
        keys$.complete()
        songs.e$.next("The")
        songs.c$.next("pri-hi-ma")
        songs.c$.complete()
        songs.d$.complete()
        songs.e$.next("End")
        songs.e$.complete()
    }
})