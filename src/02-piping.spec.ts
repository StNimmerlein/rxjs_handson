import { from, Observable, Subject } from 'rxjs'
import { reduce } from 'rxjs/operators'

type Person = {
    name: string,
    age: number,
    job?: {
        title: string,
        company: string
    }
}

const hans: Person = {
    name: 'Hans',
    age: 28,
    job: {
        title: 'Engineer',
        company: 'Energo'
    }
}
const martha: Person = {
    name: 'Martha',
    age: 34,
    job: {
        title: 'CEO',
        company: 'Energo'
    }
}
const freddy: Person = {
    name: 'Freddy',
    age: 16,
    job: {
        title: 'cashier',
        company: 'Fast\'n\'Foody'
    }
}
const jenny: Person = {
    name: 'Jenny',
    age: 14,
}
const holger: Person = {
    name: 'Holger',
    age: 54,
    job: {
        title: 'janitor',
        company: 'Rental Inc.'
    }
}

describe('combining revisited', () => {
    let persons$: Observable<Person>

    beforeEach(() => {
        // persons$ is an observable that, when subscribed to, will emit the values below:
        const values = [
            hans,
            martha,
            freddy,
            jenny,
            holger
        ]

        persons$ = from(values)
    })

    // TODO: In the following tests, assign an observable that you created by piping from persons$ to the variable obs$ to fulfill the tests


    it('return name and age as String', (done) => {
        let obs$: Observable<string>

        // ⬇️ Your code here
        // ⬆️ Your code here

        validateOutput(obs$, [
            'Hans, 28 years old',
            'Martha, 34 years old',
            'Freddy, 16 years old',
            'Jenny, 14 years old',
            'Holger, 54 years old'
        ], done)
    })


    it('only retrieve persons of full age', (done) => {
        let obs$: Observable<Person>

        // ⬇️ Your code here
        // ⬆️ Your code here

        validateOutput(obs$, [
            hans,
            martha,
            holger
        ], done)
    })


    it('only retrieve first two', (done) => {
        let obs$: Observable<Person>

        // ⬇️ Your code here
        // ⬆️ Your code here

        validateOutput(obs$, [
            hans,
            martha
        ], done)
    })


    it('first two are not needed', (done) => {
        let obs$: Observable<Person>

        // ⬇️ Your code here
        // ⬆️ Your code here

        validateOutput(obs$, [
            freddy,
            jenny,
            holger
        ], done)
    })


    it('only take last two', (done) => {
        let obs$: Observable<Person>

        // ⬇️ Your code here
        // ⬆️ Your code here

        validateOutput(obs$, [
            jenny,
            holger
        ], done)
    })

    it('only pick name property', (done) => {
        let obs$: Observable<string>

        // ⬇️ Your code here
        // ⬆️ Your code here

        validateOutput(obs$, [
            'Hans',
            'Martha',
            'Freddy',
            'Jenny',
            'Holger'
        ], done)
    })

    it('add up all ages', (done) => {
        let obs$: Observable<number>

        // ⬇️ Your code here
        // ⬆️ Your code here

        validateOutput(obs$, [
            146
        ], done)
    })

    describe('playing with time', () => {
        // Here we have a different observable. It emits the same values in the same order as the original persons$ observable, but will do so with a delay
        // See emitDelayedValues() for the timing.
        // We want to debounce this, so if a value is emitted less than 100ms after the previous one, the previous one should be omitted.
        let delayedPersons$: Subject<Person>

        beforeEach(() => {
            delayedPersons$ = new Subject<Person>()
        })

        it('debounce values', (done) => {
            let obs$: Observable<any>

            // ⬇️ Your code here
            // ⬆️ Your code here

            validateOutput(obs$, [
                martha,
                jenny,
                holger
            ], done)
            emitDelayedValues()
        })

        function emitDelayedValues() {
            setTimeout(() => delayedPersons$.next(hans), 0)
            setTimeout(() => delayedPersons$.next(martha), 50)
            setTimeout(() => delayedPersons$.next(freddy), 157)
            setTimeout(() => delayedPersons$.next(jenny), 159)
            setTimeout(() => delayedPersons$.next(holger), 300)
            setTimeout(() => delayedPersons$.complete(), 500)
        }
    })

    describe('only return values that are different then the previous value', () => {
        let observableWithDuplicates$: Observable<Person>;

        beforeEach( () => {
                observableWithDuplicates$ = from([martha, martha, jenny, holger, holger, freddy, hans, jenny]);
            }
        )

        it('should only return values that are distinct', (done) => {
            let obs$: Observable<any>;

            // ⬇️ Your code here
            
            // ⬆️ Your code here

            validateOutput(obs$, [
                martha,
                jenny,
                holger,
                freddy,
                hans,
                jenny
            ], done)
        });
    });

    describe('Adding Santa to the Observable', () => {
        const santa: Person = {
            name: 'Santa',
            age: NaN,
            job: {
                title: 'Bringer of Joy',
                company: 'Weihnachtsmann & Co. KG'
            }
        }

        it('Santa should be first value emitted', (done) => {
            let obs$: Observable<Person>

            // ⬇️ Your code here
            // ⬆️ Your code here

            validateOutput(obs$, [
                santa,
                hans,
                martha,
                freddy,
                jenny,
                holger
            ], done)
        })
    })


    it('provides possibility for side effects', (done) => {
        const sendInvitation = jest.fn()
        let obs$: Observable<Person>

        // ⬇️ Your code here
        // ⬆️ Your code here

        validateOutput(obs$, [
            hans,
            martha,
            freddy,
            jenny,
            holger
        ], () => {
            expect(sendInvitation).toHaveBeenCalledWith(hans)
            expect(sendInvitation).toHaveBeenCalledWith(martha)
            expect(sendInvitation).toHaveBeenCalledWith(freddy)
            expect(sendInvitation).toHaveBeenCalledWith(jenny)
            expect(sendInvitation).toHaveBeenCalledWith(holger)
            done()
        })
    })
})

const validateOutput = (observable: Observable<any>, expectedOutput: any[], doneCallback) => {
    observable.pipe(reduce((a, b) => a.concat(b), [])).subscribe(result => {
        expect(result).toEqual(expectedOutput)
        doneCallback()
    })
}
