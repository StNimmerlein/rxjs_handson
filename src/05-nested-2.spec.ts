import {Observable, of} from 'rxjs'

describe('resolving nested subscriptions', () => {
    it('subscribe inside subscribe', (done) => {
        // TODO: resolve this code so that it consist of only one subscription and make sure that the subscription closes after it received a value
        getUser().subscribe(username => {
            getWishlist(username).subscribe(wishlist => {
                expect(wishlist[0]).toBe('Heino\'s wish 1')
                done()
            })
        })
    })

    it('tripe nested', (done) => {
        // TODO: resolve this code so that it consist of only one subscription and make sure that the subscription closes after it received a value
        getUser().subscribe(username => {
            getCartIds(username).subscribe(cartIds => {
                getCart(cartIds[0]).subscribe(cartEntries => {
                    expect(cartEntries[0]).toBe('entry 1 of cart Heino_cart1')
                    done()
                })
            })
        })
    })

    it('combining two independent observables', (done) => {
        // TODO: resolve this code so that it consist of only one subscription and make sure that the subscription closes after it received a value
        getUser().subscribe(username => {
            getLanguage().subscribe(language => {
                getGreeting(username, language).subscribe(greeting => {
                    expect(greeting).toBe('Hi (in DE), Heino')
                    done()
                })
            })
        })
    })

    it('combining two dependent observables', (done) => {
        // TODO: resolve this code so that it consist of only one subscription and make sure that the subscription closes after it received a value
        getUser().subscribe(username => {
            getPreferredLanguage(username).subscribe(language => {
                getGreeting(username, language).subscribe(greeting => {
                    expect(greeting).toBe('Hi (in EN), Heino')
                    done()
                })
            })
        })
    })
})

function getUser(): Observable<string> {
    return of('Heino')
}

function getWishlist(username: string): Observable<string[]> {
    return of([
        `${username}'s wish 1`,
        `${username}'s wish 2`,
        `${username}'s wish 3`
    ])
}

function getCart(cartId: string): Observable<string[]> {
    return of([
        `entry 1 of cart ${cartId}`,
        `entry 2 of cart ${cartId}`,
        `entry 3 of cart ${cartId}`,
    ])
}

function getCartIds(username: string): Observable<string[]> {
    return of([
        `${username}_cart1`,
        `${username}_cart2`
    ])
}

function getLanguage(): Observable<string> {
    return of('DE')
}

function getPreferredLanguage(username: string): Observable<string> {
    return of('EN')
}

function getGreeting(username: string, language: string): Observable<string> {
    return of(`Hi (in ${language}), ${username}`)
}