import {BehaviorSubject, Subject} from 'rxjs'

describe('unsubscribing', () => {
    let a$: Subject<string>

    it('subscription needs to be unsubscribed', (done) => {
        // TODO: Fix this test
        // Task: Only the first value that is emitted from a$ should be considered, after this the subscription should unsubscribe.
        // Note: The Subject 'a' must not complete, only the subscription must end.
        a$ = new Subject<string>()

        a$.subscribe({
            next: value => {
                expect(value).toBe('a')
            },
            complete: () => done()
        })

        a$.next('a')
        a$.next('b')
    })

    it('unsubscribing in subscription', (done) => {
        // TODO: Why is this generally not a feasible way of unsubscribing? Can you imagine a scenario in which this does not work?
        // Task: First change the scenario so that this test fails with an exception (without changing the subscription)
        // Then modify the subscription to regain the desired behavior but in a secure way.

        a$ = new Subject<string>()

        const subscription = a$.subscribe(value => {
            expect(value).toBe('a')
            subscription.unsubscribe()
            done()
        })

        a$.next('a')
    })

    it('unsubscribing after subscribing', (done) => {
        // TODO: Why is this generally not a feasible way of unsubscribing? Can you imagine a scenario in which this does not works?
        // Task First change the scenario so that this test fails with a timeout (without changing the subscription)
        // Then modify the subscription to regain the desired behavior but in a secure way.

        a$ = new BehaviorSubject('a')

        a$.subscribe(value => {
            expect(value).toBe('a')
            done()
        }).unsubscribe()

    })

    it('completed subscriptions do not need to be unsubscribed', () => {
        a$ = new Subject<string>()
        const subs = a$.subscribe()

        expect(subs.closed).toBe(false)

        a$.next("a")
        expect(subs.closed).toBe(false)

        // TODO: Complete the subject/observable, so that the subscription closes automatically
        // ⬇ Your code here
        // ⬆️ Your code here

        expect(subs.closed).toBe(true)
    })

    it('errored observables do not need to be unsubscribed', () => {
        a$ = new Subject<string>()
        let errorWasEmitted = false
        const subs = a$.subscribe({
            error: () => errorWasEmitted = true
        })

        expect(subs.closed).toBe(false)

        // TODO: Send an error to the observable. You should not call a$.complete()!
        // ⬇ Your code here
        a$.error('error')
        // ⬆️ Your code here

        expect(errorWasEmitted).toBe(true)
        expect(subs.closed).toBe(true)
    })
})