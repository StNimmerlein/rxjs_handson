# Pizza and Code: RxJs Cheatsheet

### Creating Observables

* `of(...args)`: Converts arguments to observable sequence
  
* `empty()`: Creates an empty observable
  
* `from(input)`: Creates an Observable from an Array, an array-like object, a Promise, an iterable object,
  or an Observable-like object.
Creates an observable from almost anything.
  
* `interval(period)`: Creates an Observable that emits sequential numbers every specified interval of time

* `range(start, end)`: Creates an Observable that emits a sequence of numbers within a specified range.
  Delivers them synchronously.

#### Constructor: 
```js
let observable = new Observable(subscriber => {
    () => subscriber.next("Hello World")
})
```

### Useful Operators

To use operators you have to pipe into the observable 

```js
observable.pipe(
        map(value => value.getName())
)
```

* `map(val => ...)`: Applies the transform function supplied to each element in the observable

* `filter(val => ...)`: Applies filter function to each element in the Observable and strips values that
  due not meet the filter criteria.
  
* `take(count)`: Emits only the first count values emitted by the source Observable.

* `first()`: Emits only the first element.

* `last()`: Emits only the last Element

* `distinct()`: Returns an Observable that emits all items emitted by the source Observable that are distinct by comparison from previous items.

* `reduce(accumulatorFunction, seed)`: When the observable completes, returns value calculated by accumulator Function.
Accumulation starts at the seed.
  
* `tap(val => ...)`: Executes the function inside tap but does not change the observable itself.

* `debounce(time)`: Observable emits the next value only after the debounce time has passed.

### Combining Observables

* `merge(obs1, obs2)`: Creates an output Observable which concurrently emits all values from every given input Observable.

* `concat(obs1, obs2)`: Creates an output Observable which sequentially emits all values from the first given 
  Observable and then moves on to the next.
  
* `combineLatest(obs1, obs2)`: Combines multiple Observables to create an Observable whose values are calculated from 
  the latest values of each of its input Observables.
  
* `race(obs1, obs2)`: Returns the observable which emits first
  
* `forkJoin(obs1, obs2)`: Returns the last value of each observable joined as an array
  
* `zip(obs1, obs2)`: Combines values of the observables into an array
```js
zip(age$, name$, isDev$).pipe(
  map(([age, name, isDev]) => ({ age, name, isDev }))
)
.subscribe(x => console.log(x));
 
// Outputs
// { age: 27, name: 'Foo', isDev: true }
// { age: 25, name: 'Bar', isDev: true }
// { age: 29, name: 'Beer', isDev: false }
```

### Nested Observables

* `switchMap(val => ...)`: Inner observable only emits values while outer observable is not changed. Inner observable wont 
  complete when outer observable is changed but rather switch to the new value of the outer observable and start anew.
* `concatMap(val => ...)`: Merges inner and outer observable together but waits until the inner observable has completed before
  emitting the next transformation.
* `exhausMap(val => ...)`: Inner observable will always run until complete. When another outer observable would trigger the 
  inner observable while it has not completed that change in value would be ignored. 
* `mergeMap(val => ...)`: Outer and inner observable are getting merged together.
