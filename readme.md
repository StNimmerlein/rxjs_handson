### Pizza und Code: RxJs

Bevor ihr mit dem Programmieren startet, öffnet die Console von Visual Studio Code und begebt euch mit cd in den Ordner rxjs_handson.
Hier führt ihr einmal den Befehl `npm install` aus. Dadurch bekommt ihr die Packages die ihr braucht.

Vor jeder Übung werden wir euch immer kurz eine kleine Demo zeigen. 

Wir wünschen euch viel Spaß :D
